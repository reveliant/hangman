/**
 * Jeu du pendu
 * Exercices JavaScript
 */

var mots = ["JavaScript", "Programmation", "Developpement"];

// On attend d'avoir chargé le contenu de la page
window.addEventListener('DOMContentLoaded', () => {
    
    // Variables pour accéder aux champs du formulaire');
    var reponse = document.querySelector('#reponse');
    var message = document.querySelector('#message');
    var lettres = document.querySelector('#lettres');
    
    // Etapes de pendaison et initialisation
    var echecs = 0;
    var pendu = Array.from(document.querySelector('#pendu').children); // ['tete', 'corps', 'bras-gauche', 'bras-droit', 'jambe-gauche', 'jambe-droite'];
    var visagePendu = Array.from(document.querySelector('#visage-pendu').children)
    pendu.concat(visagePendu).forEach((el) => el.classList.add('hidden'));

    // On choisi un mot aléatoirement
    var mot = mots[Math.floor(Math.random() * mots.length)].toUpperCase();
    reponse.innerText = '_'.repeat(mot.length);

    // Fonction de traitement
    function tente(el) {
        var lettre = el.innerText[0];
        el.disabled = true;

        // Le mot n'a pas été trouvé et le nombre d'échecs ne dépasse pas le nombre d'étapes de pendaison
        if ((reponse.innerText != mot) && (echecs < pendu.length)) {
            // La lettre est dans le mot à trouver : on la place dans le masque
            if (mot.includes(lettre)) {
                var masque = Array.from(reponse.innerText);
                for(var index in mot) {
                    if (mot[index] == lettre) {
                        masque[index] = lettre;
                    }
                }
                reponse.innerText = masque.join('');

                // Si le mot est trouvé, on réinitialise le pendu et on affiche Gagné
                if (reponse.innerText == mot) {
                    message.innerText = "Gagné";
                }
            }
            // La lettre n'est pas dans le mot à trouver : pendaison
            else {
                // On affiche le membre suivant du pendu
                pendu[echecs].classList.remove("hidden");
                if (visagePendu[echecs].previousElementSibling != undefined) {
                    visagePendu[echecs].previousElementSibling.classList.add("hidden")
                }
                visagePendu[echecs].classList.remove("hidden");
                echecs++;

                // Si toutes les étapes de pendaison sont passées, on affiche la réponse et Perdu
                if (echecs >= pendu.length) {
                    reponse.innerText = mot;
                    message.innerText = "Perdu";
                    // document.getElementById("visage-mort").classList.remove("hidden");
                }
            }
        }
    }

    // On liste les lettres
    for (let lettre of [...Array(26)].map((_, y) => String.fromCharCode(y + 65))) {
        let button = document.createElement('button');
        button.innerText = lettre;
        button.addEventListener('click', (el) => tente(el.target));
        lettres.append(button);
    }
});